<!-- This issue is for Focus Time.  -->
<!-- NOTE: Set the issue title: Focus Time
     and assign a milestone. -->

## Focus Goal or Task

/assign me
/estimate 2h
/label ~"Values::Efficiency" ~"Activity::In-Progress"
/weight 1
/due Thursday
/milestone %