<!-- This issue is for Greenhouse technical assessments or interviews.  -->
<!-- NOTE: Please assign a Greenhouse label (format `greenhouse::<stage_name>`)
     and set a milestone. -->

/assign @dcoy
/estimate 1h
/due in 2 days
